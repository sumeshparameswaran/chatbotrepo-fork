import React from "react";
import { Button } from "reactstrap";

import Modal from 'react-bootstrap/Modal'


function Modalpopup(props) {
    return (<Modal show = {props.modalOpened} size="sm" style = {{borderRadius : "5px"}}
    aria-labelledby="contained-modal-title-vcenter"
    centered dialogClassName="modal-90w">
        <Modal.Dialog centered>
        <Modal.Header>
          <Modal.Title style = {{color : "red"}}>Warning</Modal.Title>
        </Modal.Header>
      
        <Modal.Body>
          <p>Do you want to delete the project</p>
        </Modal.Body>
      
        <Modal.Footer>
          <Button variant="secondary" onClick = {props.hidePopUp}>Close</Button>
          <Button variant="primary primary-btn" onClick = {props.deleteProject}>Delete</Button>
        </Modal.Footer>
      </Modal.Dialog>
     </Modal>);
}

export default Modalpopup