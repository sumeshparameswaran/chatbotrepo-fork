import React, { Component } from "react";
import TextareaAutosize from "react-textarea-autosize";
import { Button } from "reactstrap";
import { FaTimes } from "react-icons/fa";

class MessageBot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onClose = this.onClose.bind(this);
  }
  handleChange(event) {
    const input = event.target.value;
    this.setState({ message: input });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.props.handleMessage(this.state.message, false);
  }
  onClose() {
      this.props.handleClose();
  }
  render() {
    return (
      <div style={{ borderRadius: 5, backgroundColor: "#28a745" }}>
        <div style={{ paddingLeft: 5, display:"flex", justifyContent:"space-between" }}>

          <div>Bot</div>
          <FaTimes color="black" style={{paddingTop:5}} onClick={this.onClose}></FaTimes>

        </div>
        <div style={{ backgroundColor: "white", padding: 10, height: "auto" }}>
          {/* <Input
            type="text"
            style={{ height: 20, fontSize: 15, width: 150 }}
            placeholder="message"
          /> */}
          <TextareaAutosize
            style={{ fontSize: 15, width: 200 }}
            value={this.state.message}
            placeholder="Message"
            onChange={this.handleChange}
          />
          <Button size="sm" onClick={this.handleSubmit}>
            Add
          </Button>
        </div>
      </div>
    );
  }
}

export default MessageBot;
