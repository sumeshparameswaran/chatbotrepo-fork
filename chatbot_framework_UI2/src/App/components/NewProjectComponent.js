import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { Form } from "reactstrap";
import { FormGroup } from "reactstrap";
import { Label } from "reactstrap";
import { Input } from "reactstrap";
import { Button } from "reactstrap";
import SharedTextArea from "./SharedTextArea";
import CreateUseCase from "./CreateUseCaseComponent";
import { connect } from "react-redux";
import * as actions from "../../store/newproject";
import { Redirect } from "react-router-dom";

import Spineerround from "./util/Spinner";

class NewProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
     formError : false,
      errors :{
        projectNameError :''
      },
      data: {
        projectname: "",
        description: "",
        showComponent: false,
      },
    };

    if(this.props.history.state !== undefined) {
      this.setState({data : this.props.history.state})
    }
    this._onButtonClick = this._onButtonClick.bind(this);
  }
  handleInputChange = (event) => {
    console.log("hello");
    const formdata = this.state.data;
    formdata[event.target.name] = event.target.value;
    this.setState({ data: formdata });
     if("projectname" === event.target.name && this.state.data.projectname.length>1){
       this.setState({errors:{projectNameError:''}})
       this.setState({formError:false})
    }
    
  };

   validate =()=> {
    let isformError = false;
    if(this.state.data.projectname.length<=0) {
     const errors = {projectNameError :"please enter the projectname"}
      this.setState({errors:errors});
      isformError = true;
      this.setState({formError:isformError})
    }
    return isformError;

   }
  handleSubmit = (event) => {
    const values = this.state.data;
    console.log(values.description);
    console.log(values.projectname);
    
    if(this.validate()) {
      return true;
    }
    event.preventDefault();
    
    debugger;
    this.props.projectData(values.projectname, values.description);
        this.props.history.push("/dashboard/default");
  };
  _onButtonClick = (event) => {
    if(this.validate()) {
      return true;
    }
    this.setState({
      showComponent: true,
    });
  };
  renderRedirect = () => {
    if (this.state.showComponent) {
     return <Redirect to="/newUseCase" />;
    }
  };

  componentDidUpdate() {
//this.setState({data : this.props.history.state})
  }

  render() {
    return (
      <div>
        {this.props.loading?<Spineerround/>:
      <div className="container">
        <div>
          <Label sm={2}>Name</Label>
          <Col sm={5}>
            <Input
              name="projectname"
              type="text"
              value={this.state.data.projectname}
              onChange={this.handleInputChange}
            />
          </Col>
            <div>
              {this.state.formError ? <div className ="text-danger" style = {{left:"30%",marginLeft:"2%"}}>{this.state.errors.projectNameError} </div> : <div></div>} 
            </div>
          <Col sm={3}>
            {/* <SharedTextArea
              name="description"
              value={this.state.data.description}
              onChange={this.handleInputChange}
            /> */}
            <div className="form-group">
              <label htmlFor="descField" className="col-xs-5">
                Description
              </label>
              <div className="col-xs-10">
                <textarea
                  type="text"
                  className="form-control"
                  id="descField"
                  name="description"
                  value={this.state.data.description}
                  onChange={this.handleInputChange}
                ></textarea>
              </div>
            </div>
          </Col>
        </div>
        <div
          className="col-xs-10 col-xs-offset-2"
          style={{
            height: "25%",
            width: "25%",
          }}
          align="right"
        >
          <button
            style={{
              margin: "5px",
            }}
            type="submit"
            onClick={this.handleSubmit}
            className="btn btn-primary"
          >
            Save
          </button>
          <button
            style={{
              margin: "5px",
            }} 
            type="submit"
            onClick={this._onButtonClick}
            className="btn btn-primary"
          >
            Next
          </button>
          {this.renderRedirect()}
        </div>
      </div>
  }
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isShowComponent: state.showComponent,
    loading : state.loading,
    error : state.error,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    projectData: (projectname, description) =>
      dispatch(actions.saveProject(projectname, description)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(NewProject);
