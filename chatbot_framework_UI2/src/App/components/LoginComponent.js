import React from "react";
// import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../store/auth";

// import './../../../assets/scss/style.scss';
import "./../../assets/scss/style.scss";
// import Aux from "../../../hoc/_Aux";
import Aux from "../../hoc/_Aux";
//import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
// import Breadcrumb from "../../App/layout/AdminLayout/Breadcrumb";
import Spineerround from "./util/Spinner";

class SignUp1 extends React.Component {
  constructor(props) {
      super(props)
  }
  state = {
    isformError : false,
    usernameError :'',
    passwordError : '',
    credential: {
      username: "",
      password: "",
    },
  };

  handleInputChange = (event) => {
    const cred = this.state.credential;
    const {name,value} = event.target;
    cred[name] = value;
    this.setState({ credential: cred });
    switch(name) {
      case 'username' :
          if(this.state.credential.username.length<3) {
            this.setState({usernameError:'pleae enter username'})
          }else {
            this.setState({usernameError:''})
          }
      case 'password' :
          if(this.state.credential.password.length<3) {
              this.setState({passwordError:'pleae enter password'})
            }else {
              this.setState({passwordError:''})
          }
           

    }
  };

  validateform = (values)=>{
    let isformError = false;

    if(values.username.length<=3) {
      this.setState({usernameError:'pleae enter username'})
      isformError = true;
    }if(values.password.length<=3) {
      this.setState({passwordError:'pleae enter password'})
      isformError = true;
    }
    this.setState({isformError : isformError})
    return isformError;
  }
  handleSubmit = (event) => {
    const values = this.state.credential;
    if(this.validateform(values)) {
      return true;
    }
    this.props.onAuth(values.username, values.password);
  };

  render() {
    return (
      
      <Aux>
        {/* <Breadcrumb /> */}
        <div>
        {this.props.loading ?<Spineerround/>:
        
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="auth-bg">
              <span className="r" />
              <span className="r s" />
              <span className="r s" />
              <span className="r" />
            </div>
            
            <div className="card">
            <div className="container" style = {{color:"red", 
        
        position : "absolute", left : "15%" ,top :"0%"
        }}>{this.props.error?<p>please eneter correct credentials {this.props.error}</p>:''}</div>
              <div className="card-body text-center">
                <div className="mb-4">
                  <i className="feather icon-unlock auth-icon" />
                </div>
                <h3 className="mb-4">Login</h3>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="username"
                    value={this.state.credential.username}
                    onChange={this.handleInputChange}
                    name="username"
                  />
                </div>
                <div>
                  {this.state.isformError ? <div className ="text-danger" style = {{left:"30%",marginLeft:"2%",marginBottom:"2%",flexDirection: "column-reverse",display:"flex"}}>{this.state.usernameError} </div> : <div></div>} 
                </div>
                <div className="input-group mb-4">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="password"
                    value={this.state.credential.password}
                    onChange={this.handleInputChange}
                    name="password"
                  />
                </div>
                <div>
                  {this.state.isformError ? <div className ="text-danger" style = {{left:"30%",marginLeft:"2%"  ,marginBottom:"2%"}}>{this.state.passwordError} </div> : <div></div>} 
                </div>
                <div className="form-group text-left">
                  <div className="checkbox checkbox-fill d-inline">
                    <input
                      type="checkbox"
                      name="checkbox-fill-1"
                      id="checkbox-fill-a1"
                    />
                    <label htmlFor="checkbox-fill-a1" className="cr">
                      {" "}
                      Save credentials
                    </label>
                  </div>
                </div>
                <button
                  className="btn btn-primary shadow-2 mb-4"
                  onClick={this.handleSubmit}
                >
                  Login
                </button>
                {/* <p className="mb-2 text-muted">
                  Forgot password?{" "}
                  <NavLink to="/auth/reset-password-1">Reset</NavLink>
                </p>
                <p className="mb-0 text-muted">
                  Don’t have an account?{" "}
                  <NavLink to="/auth/signup-1">Signup</NavLink>
                </p> */}
              </div>
            </div>
          </div>

        </div>
  }
        </div>
      </Aux>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (username, password) =>
      dispatch(actions.authLogin(username, password)),
  };
};

export default connect(null, mapDispatchToProps)(SignUp1);
