import React, { Component, Suspense } from "react";
import { Switch, Route } from "react-router-dom";

import "../../../node_modules/font-awesome/scss/font-awesome.scss";

import Loader from "./Loader";
import Aux from "../../hoc/_Aux";
import ScrollToTop from "./ScrollToTop";
import routes from "../../route";
import Loadable from "react-loadable";

class MenuComponent extends Component {
  state = {};
  render() {
    const AdminLayout = Loadable({
      loader: () => import("./AdminLayout"),
      loading: Loader,
    });
    const menu = routes.map((route, index) => {
      return route.component ? (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          name={route.name}
          render={(props) => <route.component {...props} />}
        />
      ) : null;
    });

    return (
      <Aux>
        <ScrollToTop>
          <Suspense fallback={<Loader />}>
            <Switch>
              {menu}
              <Route path="/" component={AdminLayout} />
            </Switch>
          </Suspense>
        </ScrollToTop>
      </Aux>
    );
  }
}

export default MenuComponent;
